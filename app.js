var express = require('express');
var app = express(); //Inicializa o express
var server = require('http').createServer(app); //Cria o servidor
var io = require('socket.io').listen(server);
var usuarios = {};

server.listen(3000); //Porta do servidor

app.get('/', function(req, resp){
	//As requisições do servidores serao redirecionadas para o HTML abaixo
	resp.sendFile(__dirname + '/publico/index.html');
});

io.sockets.on("connection", function(socket){
	socket.on("novo usuario", function(nickname, callback){

		if(nickname in usuarios){ //Verifica se o usuario já esta no chat
			callback({retorno: false, msg: "Nickname em uso! Escolha outro"});
		} else {
			//Novo usuario no chat
			callback({retorno: true, msg: ""});
			socket.nickname = nickname;
			usuarios[socket.nickname] = socket;
			atualizaUsuarios();
		}
	});


	socket.on("enviar mensagem", function(data){
		//enviando mensagem
		var mensagem = data.trim();

		var primeiraLetra = mensagem.substring(0, 1);
		if(primeiraLetra == "/"){
			//É mensagem privada
			var nome = mensagem.substr(1, mensagem.indexOf(" ")).trim();
			var msg = mensagem.substr(mensagem.indexOf(" ")+1); //Msg é a mensagem sem o "/usuario" na frente
			if(nome in usuarios){
				sucketUsuario = usuarios[nome];

				//Envia mensagem privada ao usuário
				sucketUsuario.emit("nova mensagem", {msg: "(mensagem privada de " + socket.nickname+ "): <i>"+msg+"</i>", 
													 nick: sucketUsuario.nickname  });

				//Envia mensagem para mim mesmo
				socket.emit("nova mensagem", {msg: "(você enviou uma mensagem privada para "+ nome +"): <i>"+msg+"</i>",
											  nick: sucketUsuario.nickname });
			}else{
				//Usuario não encontrado
				socket.emit("nova mensagem", {msg: "O usuário " + nome + " não foi encontrado ",
											  nick: socket.nickname} );
			}
		}else{
			io.sockets.emit("nova mensagem", {msg: mensagem, nick: socket.nickname});
		}
	});

	//recebe evento do usuario que saiu da pagina
	socket.on("disconnect", function(){
		if(socket.nickname){
			//Só faz alguma coisa se o usuario tiver um nickname associado a ele caso contrario não faz nada
			io.sockets.emit("nova mensagem", {msg: "O usuário " + socket.nickname + " saiu do chat..... ", nick: socket.nickname} );
			delete usuarios[socket.nickname];
			atualizaUsuarios();
		}
	});

	function atualizaUsuarios(){
		io.sockets.emit('atualiza usuarios', Object.keys(usuarios) );
	};

});